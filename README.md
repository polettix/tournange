# tournange - Tournament Arrange

`tournange` is a simple program to help generating schedules for board game
tournaments, where each game hosts more than two players.

Use it like this:

```shell
$ ./tournange 3
round 1:
  (*1, *2, *3)
  (*5,  7,  9)
  (*4,  6,  8)

round 2:
  (*1, *4, *5)
  (*3,  7,  8)
  (*2,  6,  9)

round 3:
  (*1,  6,  7)
  (*2, *5,  8)
  (*3, *4,  9)

round 4:
  (*1,  8,  9)
  (*3, *5,  6)
  (*2, *4,  7)

```

The output shows the rounds (4 in the example) as well as the matches that
belong to each round. In games on [BoardGameArena][] that require at least one
premium player for each match, the asterisk indicates which player identifiers
have to be associated to a premium player.

The match assembly is based on affine planes generated from corresponding
projective planes. For this reason, you cannot have a *precise*
arrangement for six people in a match (as well as any other integer that
is not a prime or a power of a prime); in this case, you will have to
further decide for any of three sub-optimal alternatives:

```shell
$ ./tournange 6
For sixtets, please specify one of 'base', '7ok', or 'dup'

$ ./tournange 6 base
#...
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[Aquarium]: https://www.puzzle-aquarium.com/
[ETOOBUSY]: https://github.polettix.it/ETOOBUSY/
[aquarium puzzle game]: https://github.polettix.it/ETOOBUSY/tagged/#aquarium%20puzzle%20game
